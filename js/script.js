  const resultsContainer = document.getElementById('results');
  const mortgageForm = document.getElementById('mortgageForm');
  const loanToValueSpan = document.getElementById('loanToValue');
  const loanToValueWrapper = document.getElementById('loanToValueWrapper');
  const propertyValue = document.getElementById('propertyValue');
  const deposit = document.getElementById('deposit');
  const termYears = document.getElementById('termYears');
  
  
  propertyValue.value = '250000';
  deposit.value = '100000';
  termYears.value = '25';
  
  function updateLoanToValue() {
      const formData = new FormData(mortgageForm);
      const propertyValue = parseFloat(formData.get('propertyValue')) || 0;
      const deposit = parseFloat(formData.get('deposit')) || 0;
  
      if (propertyValue && deposit) {
          const mortgageAmount = propertyValue - deposit;
          const loanToValue = Math.round((mortgageAmount / propertyValue) * 100);
  
          loanToValueSpan.textContent = loanToValue;
          loanToValueSpan.style.display = 'inline';
          loanToValueWrapper.style.display = 'block';
      } else {
          loanToValueSpan.style.display = 'none';
          loanToValueWrapper.style.display = 'none';
      }
  }
  
  function handleFormSubmit() {
      const formData = new FormData(mortgageForm);
      const payload = {};
  
      const loadingMessage = 'Loading...';
  
      document.querySelectorAll('.widget').forEach(widget => {
          const monthlyPaymentElement = widget.querySelector('.data-item-value-monthly-payment');
          const initialRateElement = widget.querySelector('.data-item-value-initial-rate');
          const dealEndsElement = widget.querySelector('.data-item-value-deal-ends');
  
          if (monthlyPaymentElement) monthlyPaymentElement.textContent = loadingMessage;
          if (initialRateElement) initialRateElement.textContent = loadingMessage;
          if (dealEndsElement) dealEndsElement.textContent = loadingMessage;
      });
  
      formData.forEach((value, key) => {
          payload[key] = value;
      });
  
      const splitPurposeAndType = payload.purpose.split('-');
  
      payload.mortgageAmount = payload.propertyValue - payload.deposit;
      payload.termMonths = payload.termYears * 12;
      payload.purpose = splitPurposeAndType[0];
      payload.type = splitPurposeAndType[1];
  
      delete payload.deposit;
      delete payload.termYears;
  
      fetch('https://api.myac.re/v1/acre/local-sourcing/bestbuys', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: new URLSearchParams(payload),
      })
          .then(response => response.json())
          .then(data => {
              // Clear previous results
              resultsContainer.innerHTML = '';
              
              const sortedArray = data.sort((a,b) => a.initialPeriod - b.initialPeriod);
              
              sortedArray.forEach((item, index) => {
                  const resultElement = document.createElement('div');
                  resultElement.classList.add('widget')
                  
                  const date = new Date();
  
                  date.setMonth(date.getMonth() + item.initialPeriod);
  
                  const dealEnds = date.toLocaleDateString();
  
                  const twoYearFixInitialPeriods = [24, 25, 26, 27, 28]
                  const threeYearFixInitialPeriods = [36, 37, 38, 39, 40]
                  const fiveYearFixInitialPeriods = [62, 63, 64, 65, 66]
  
                  resultElement.innerHTML = `
                      <div class="widget-top">
                        <div class="widget-top-left">
                        <svg width="34" height="48" viewBox="0 0 34 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M25.1309 40.651L20.5767 46.4813L11.4269 21.3423L23.7154 16.8696L32.8653 42.0086L25.629 40.4698L25.3233 40.4048L25.1309 40.651Z" fill="white" stroke="#344054"/>
                          <path d="M9.22933 40.4698L1.99299 42.0086L11.1428 16.8696L23.4314 21.3423L14.2816 46.4813L9.72736 40.651L9.53499 40.4048L9.22933 40.4698Z" fill="white" stroke="#344054"/>
                          <circle cx="17" cy="17.5" r="16.5" fill="#F9FAFC" stroke="#344054"/>
                          <circle cx="17" cy="17.5" r="11.5" fill="white" stroke="#344054"/>
  
                          ${twoYearFixInitialPeriods.includes(item.initialPeriod) && `<path d="M17.9707 14.3333C17.7383 14.0553 17.4056 13.9163 16.9727 13.9163C16.3802 13.9163 15.9769 14.1373 15.7627 14.5793C15.6396 14.8346 15.5667 15.2402 15.5439 15.7961H13.6504C13.6823 14.953 13.835 14.2717 14.1084 13.7522C14.6279 12.7633 15.5508 12.2688 16.877 12.2688C17.9251 12.2688 18.7591 12.5605 19.3789 13.1438C19.9987 13.7226 20.3086 14.4905 20.3086 15.4475C20.3086 16.1812 20.0898 16.8329 19.6523 17.4026C19.3652 17.7808 18.8936 18.2024 18.2373 18.6672L17.458 19.2209C16.9704 19.5673 16.6354 19.818 16.4531 19.9729C16.2754 20.1278 16.125 20.3079 16.002 20.5129H20.3291V22.2288H13.541C13.5592 21.5178 13.7119 20.8684 13.999 20.2805C14.277 19.6197 14.9333 18.9202 15.9678 18.1819C16.8656 17.5393 17.4466 17.079 17.7109 16.801C18.1165 16.3681 18.3193 15.8941 18.3193 15.3792C18.3193 14.9599 18.2031 14.6112 17.9707 14.3333Z" fill="#344054"/>`}
  
                          ${threeYearFixInitialPeriods.includes(item.initialPeriod) && `<path d="M15.7832 14.2922C15.5645 14.5839 15.4596 14.9736 15.4688 15.4612H13.6504C13.6686 14.969 13.7529 14.5019 13.9033 14.0598C14.0628 13.6724 14.3135 13.3147 14.6553 12.9866C14.9105 12.7542 15.2135 12.5764 15.5645 12.4534C15.9154 12.3303 16.346 12.2688 16.8564 12.2688C17.8044 12.2688 18.5677 12.5149 19.1465 13.0071C19.7298 13.4947 20.0215 14.151 20.0215 14.9758C20.0215 15.5592 19.8483 16.0514 19.502 16.4524C19.2832 16.703 19.0553 16.8739 18.8184 16.9651C18.9961 16.9651 19.2513 17.1178 19.584 17.4231C20.0807 17.8834 20.3291 18.5123 20.3291 19.3098C20.3291 20.1484 20.0374 20.8866 19.4541 21.5247C18.8753 22.1581 18.0163 22.4749 16.877 22.4749C15.4733 22.4749 14.498 22.0168 13.9512 21.1008C13.6641 20.6132 13.5046 19.9752 13.4727 19.1868H15.3867C15.3867 19.5833 15.4505 19.9114 15.5781 20.1711C15.8151 20.6497 16.2458 20.8889 16.8701 20.8889C17.2529 20.8889 17.5856 20.759 17.8682 20.4993C18.1553 20.2349 18.2988 19.8567 18.2988 19.3645C18.2988 18.7128 18.0345 18.2776 17.5059 18.0588C17.2051 17.9358 16.7311 17.8743 16.084 17.8743V16.4797C16.7174 16.4706 17.1595 16.4091 17.4102 16.2952C17.8431 16.1038 18.0596 15.7164 18.0596 15.1331C18.0596 14.7548 17.9479 14.4472 17.7246 14.2102C17.5059 13.9732 17.196 13.8547 16.7949 13.8547C16.3346 13.8547 15.9974 14.0006 15.7832 14.2922Z" fill="#344054"/>`}
  
                          ${fiveYearFixInitialPeriods.includes(item.initialPeriod) && `<path d="M16.8359 17.3069C16.599 17.3069 16.3939 17.3365 16.2207 17.3958C15.9154 17.5051 15.6852 17.7079 15.5303 18.0042L13.7803 17.9221L14.4775 12.4465H19.9395V14.1008H15.8857L15.5303 16.2678C15.8311 16.0719 16.0658 15.942 16.2344 15.8782C16.5169 15.7734 16.861 15.7209 17.2666 15.7209C18.0869 15.7209 18.8024 15.9967 19.4131 16.5481C20.0238 17.0995 20.3291 17.9016 20.3291 18.9543C20.3291 19.8704 20.0352 20.6884 19.4473 21.4084C18.8594 22.1285 17.9798 22.4885 16.8086 22.4885C15.8652 22.4885 15.0905 22.2356 14.4844 21.7297C13.8783 21.2239 13.541 20.5061 13.4727 19.5764H15.4141C15.4915 20.0002 15.6396 20.3284 15.8584 20.5608C16.0771 20.7887 16.3962 20.9026 16.8154 20.9026C17.2985 20.9026 17.6654 20.734 17.916 20.3967C18.1712 20.0549 18.2988 19.6265 18.2988 19.1116C18.2988 18.6057 18.1803 18.1796 17.9434 17.8333C17.7064 17.4823 17.3372 17.3069 16.8359 17.3069Z" fill="#344054"/>`}
                        </svg>
                          <h2 class="item-title">${item.title}</h2>
                        </div>
                        <button>Check eligibility</button>
                      </div>
                      <div class="widget-bottom">
                        <div class="data-item">
                          <span class="data-item-header">Monthly repayment</span>
                          <span class="data-item-value data-item-value-monthly-payment">£${item.monthlyPayment}</span>
                        </div>
                        <div class="data-item">
                          <span class="data-item-header">Interest rate deal</span>
                          <span class="data-item-value data-item-value-initial-rate">${item.initialRate}%</span>
                        </div>
                        <div class="data-item">
                          <span class="data-item-header">Deal ends</span>
                          <span class="data-item-value data-item-value-deal-ends">${dealEnds}</span>
                        </div>
                      </div>
                  `;
  
                  resultsContainer.appendChild(resultElement);
              });
          })
          .catch(error => {
              console.error('Error:', error);
          });
  }
  
  ['propertyValue', 'deposit'].forEach(fieldName => {
    const inputField = document.getElementById(fieldName);
    inputField.addEventListener('input', updateLoanToValue);
  });

  mortgageForm.addEventListener('submit', function (event) {
    event.preventDefault();
    handleFormSubmit();
  });

  // Call handleFormSubmit on page load
  handleFormSubmit();
  updateLoanToValue();
